---
title: Godot projeto cawboy
subtitle: Game Western
date: 2017-03-07
tags: ["example", "gamedev"]


#### Craindo o mundo e adcionando o player
Add Nó principal > rename: World > Add filho > kinematic body2d > sprite > rename player >
arrastar: texturas > lado ossinho > grid "..." > 32x32 > view > show grid

### Funcao que vai interagir com o loop do usuario
func _physics_process(delta):

#### Se a teclado teclado for pressionada vai pra direita
if Input.is_action_pressed("ui_right"):


#### Movendo em horizontal "x"
MOTION.x = 100


#### Criando a variavel com "Vector2", e ele pode add dois numeros "floats" a uma variavel
var MOTION = Vector2()


#### Funcao que pede o "Vector2", e o numero "100" se movimenta no eixo x
move_and_slide(MOTION)

#### Para parar
else:
	MOTION.x = 0


#### Ir pra direit
elif Input.is_action_pressed("ui_left"):
		MOTION.x = -100

#### Personagem cair
MOTION.y += 20

#### Criar um cara parado (chao)
Node principal > add Staticbody2d > criar sprite > arrastar textura >
visility> preto > agrupar itens(barra tarefas)

#### Add colision
player/chao > colisionShape2d > retangulo > ajustar > Renomear chao > wall

#### Multiplicar a plataforma e Organizar as waals
crt + D > node > add > rename: walls, arrastar


#### No script do player, fazer o Boneco pular

#### Se estiver encostando no chao
if is_on_floor():

#### E se "action" for "Up" que eh a tecla pra cima
if Input.is_action_just_pressed("ui_up"):

#### Subindo 300 quadrados
MOTION.y = -300


## Agora especificar onde esta o chao

#### A parte superior eh negativa em "godot" o "-1" é o ceu
const UP = Vector2( 0, -1 )

#### Chamando
move_and_slide(motion, UP)


#### Criando variaveis
const SPEED = 200
const GRAVITY = 20
const JUMP_HEIGHT = 550


#### MUdas as variaveis pelas const
motion.x = SPEED 		
motion.x = -SPEED
motion.y = JUMP_HEIGHT

#### Add "Motion" pra funcao zerar a contagem e a queda ficar perfeita
motion = move_and_slide(motion, UP)


#### Transformar o "PNG" em cho
Criar novo node > reomear: TileGrass > add sprite > arrastar textura > importar > 2d pixel

####
off set > center: desmarcar > region > desmarcar > nome > grid snap

####
clicar pixel > Staticbody2d > colisionShape2d > retangle > 16 x 16 > snap > agrupar

####
Arrastar o elemento > jogar pra frente > fazer novamente com o 2


####
world > add no filho > tileMap > arrastar glass.tres > tileSet > size > 16 x 16 > apagar;bota direito

#### Camera acompanhar
botao "+" no personagem > camera 2d > current


####
selecionar player > importar > 2D > REIMPORTAR

####
botao direito sprite > converter tipo > animate spriter

##
frame > new sprite frame > clicar sprite frame > run

#### Para controlar um algo só usar o "$"
$Sprite.play("run")

#### Converter tipo sprite		
sprite > animate sprite

####
sprites run, idle, jumo

#### Nao flipar pra esquerda
$Sprite.flip_h = false

#### Flipar pra direita
$Sprite.flip_h = false

#### TOda vez que nao tiv erno chao aparece a funcao de pular
else:
   $Sprite.play("jump");

####
add nova cena >Paralalax background > jogar pracima > layer paralalx > sprite > arrastar texture>
snap/scale > preencher a tela toda

####
Paralelax > minorring > 1024 > embaixo 512 > motion > scale > 0.1 / 0.1

#### Criar inimigo
novo no > stetickbody2d
