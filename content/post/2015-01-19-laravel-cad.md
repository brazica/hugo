---
title: Laravel O super framework
subtitle: Best framework!
date: 2015-01-19
---

#### Criando projeto
$ composer create-project --prefer-dist "laravel/laravel" z.Cad2

####
$ sudo npm install
$ sudo npm run dev


#### Preparar layout
index > criar pasta laoyt > mudar rota > token

### incrementar o index
bootstrap criar navbar


## Criar modelos

#### Dentro da tabela  produtos temos uma dependencia de categorias, pq vamos ter um campo categoria lah, entao por isso criamos criamos a primeira tabela caegoria e o modelo e o "migration

plataform / game

$ php artisan make:model Category -m
$ php artisan make:model Product -m

#### Em categorias add
$table->string('name');

#### Em produtos
$table->string('name');
$table->integer('stock');
$table->float('price');
$table->integer('categories_id')->unsigned();
$table->foreign('categories_id')->references('id')->on('categories');


####  Configurar o "env"
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=achievs
DB_USERNAME=rx
DB_PASSWORD=pLe1978


####
$ php artisan migrate:fresh


####
$ php artisan make:controller ControladorCategorie --resource
$ php artisan make:controller ControladorProduct --resource

####Arrumar rotas e retirar as variaveis de ambiente
Route::get('/produtos', 'ContoladorProduto@index');
Route::get('/categorias', 'ContoladorCategoria@index');

#### Nova categoria
Route::get('/categorias/novo', 'ControladorCategorie@create');


#### Importar Categoria
use App\Category;


#### 13 API - Carregando os campos "Categoria" atraves da rota "Api"

#### Em routes > API vamos retornar as "Plataformas" em formato JSon
Route::get('/plataform', 'ControladorPlataform@indexJson');


#### No controlador categoria > final da linha: vamos retornar um "JSON"
public function indexJson()
{
    $plata = Plataform::all();
    return json_encode( $plata ); //retornando "json_encode"
}

#### Pegando os dados da "plataform"
http://localhost:8000/api/plataform

[{"id":2,"name":"PSV","created_at":"2018-11-12 22:07:28","updated_at":"2018-11-12 22:08:24"},{"id":3,"name":"PSV","created_at":"2018-11-12 22:08:31","updated_at":"2018-12-17 16:17:45"},{"id":4,"name":"PS4","created_at":"2018-11-12 22:08:37","updated_at":"2018-11-12 22:08:37"}]


#### Recuperando o "array" de cima, se imprimiu na tela ta tudo certo
$.get('/api/plataform', function(data) {
        console.log(data);         
});

[{"id":2,"name":"PSV","created_at":"2018-11-12 22:07:28","updated_at":"2018-11-12 22:08:24"},{"id":3,"name":"PSV","created_at":"2018-11-12 22:08:31","updated_at":"2018-12-17 16:17:45"},{"id":4,"name":"PS4","created_at":"2018-11-12 22:08:37","updated_at":"2018-11-12 22:08:37"}]


Route::resource('/games', 'ControladorGame');



## Criando APi pra retornar produtos

#### Trocar o "index" em routes pora "indexView"
Route::get('/games', 'ControladorGame@indexView');


#### EM "API" acrecentar:
Route::resource('/games', 'ControladorGame');


#### Ad no controlador produtos

==== *Aqui vai retoranar produtos
public function indexView()
{
    return view('games');
}

=======*Aqui vai retornar outra coisa
public function index()
{
    $game = Game::all();
    return $game->toJson();
    //$game = Game::with('plataform')->get();

  }

  #### Importar
  use App\Game;



#### Montar a categoria com os dados acima
function loadPlataform() {
  $.getJSON('/api/plataform', function(data) {
    for (i = 0; i < data.length; i++) {
        opcao = '<option value ="' + data[i].id + '">' +
            data[i].name + '</option>';
            $("#plataformGame").append(opcao);
          }
        });
      }



####
linhas = $("#tableGames>tbody>tr");
e = linhas.filter(function(i, e) {
    return e.cells[0].textContent == 1;
});

e[0].cells[0]

e = linhas.filter(function(i, e) {
                  return e.cells[0].textContent == game.id;
              });
              if (e) {
                e[0].cells[0].textContent = game.id;
                e[0].cells[1].textContent = game.pwd;
                e[0].cells[2].textContent = game.name;
                e[0].cells[3].textContent = game.online;
                e[0].cells[4].textContent = game.time;
                e[0].cells[5].textContent = game.indication;
                e[0].cells[6].textContent = game.comments;
                e[0].cells[7].textContent = game.plataform_id;
              }
            },
            error: function(error) {
              console.log(error);
            }
          });
      }







#### Litar rotas
$ php artisan route:list


####

game =  { pwd: 12, name: "bbzao10", online: "sim", time: 12, indication: "gg", comments: "renam", plataform_id: 1}

$.post('/api/games', game, function(data) {
    console.log(data);
});


## Tinker

###
$ php artisan tinker


>>> $p = new App\Game()
>>> $p->name = "bbzao"
=> "bbzao"
>>> $p->pwd = 10
=> 10
>>> $p->online = "sim"
=> "sim"
>>> $p->time = 24
=> 24
>>> $p->indication = "gg"
=> "gg"
>>> $p->comments = "gg"
=> "gg"
>>> $p->plataform_id = 6
=> 6
>>> $p->save()
=> true


## salvando produto

#### PRocurar "store" pra salvar
$ php artisan route:list

#### Em controlador "game"
    $game = new Game();
    $game->pwd = $request->input('pwd');
    $game->name = $request->input('name');
    $game->online = $request->input('online');
    $game->time = $request->input('time');
    $game->indication = $request->input('indication');
    $game->comments = $request->input('comments');
    $game->plataform_id = $request->input('plataform_id');
    $game->save();
    return json_encode($game);

#### Criando uma rota
Route::get('/categorias', function() {
  return 'teste';

#### Acessando a rota
http://127.0.0.1:8000/api/categorias

#### Retornar JSOn
Route::get('/categorias', 'ControladorCategorie@indexJson');

####
public function indexJson()
{
    $cats = Category::all();
    return json_encode($cats);
}

####
cat = [{"id":2,"name":"cu","created_at":"2018-11-22 09:27:47","updated_at":"2018-11-22 09:46:41"},{"id":3,"name":"ff","created_at":"2018-11-22 09:31:00","updated_at":"2018-11-22 09:46:35"}]

####
cat[0]
Object { id: 2, name: "cu", created_at: "2018-11-22 09:27:47", updated_at: "2018-11-22 09:46:41" }

#### Carrrgar as categorias por "Ajax" no "Select"
$.getJSON('/api/categorias', function(data) {
   console.log(data);
});

#### Novo produto
function novoProduto() {
    $('#id').val('');
    $('#nomeProduto').val('');
    $('#precoProduto').val('');
    $('#qtdProduto').val('');
    $('#dlg_produtos').modal('show');
  }


#### Caregar categorias
function carregarCategorias() {
  $.getJSON('/api/categorias', function(data) {
      for(i=0; i <data.length;i++) {
          opcao = '<option value ="' + data[i].id + '">' +
             data[i].nome + '</option>';
           $('#categoriaProduto').append(opcao);
         }
    });
  }

#### Trocar em rotas o "index" produtos pelo "indexView"
Route::get('/produtos', 'ContoladorProduto@indexView');

####
Route::resource('/produtos', 'ControladorProduct');

#### Em produtos
use App\Product;

####
public function indexView()
{
    return view('/produtos');
}

public function index()
{
    $prods = Produto::all();
    return $prods ->toJson();
}

####
$ php artisan tinker

####

$p = new App\Product()
$p ->name = "Aparelho de DVD"
$p ->stock = 6
$p ->price = 500;
$p ->categories_id = 1;
$p ->categories_id = 2;
>>> $p->save()

####
$.getJSON('/api/produtos', function(products) {
      for(i = 0; i < products.length; i++ ) {
      console.log(products[i]);
    }
});

#### Acessando o "ID" da tabela

$('#tableGames')

####
$("#tableProduct>tbody").append('teste');

####
function loadProducts() {
    $.getJSON( '/api/produtos', function( products ) {
        for( i = 0; i < products.length; i++ ) {
          line = moutLine( products[ i ] );
          $( '#tableProduct>tbody' ).append( line );
      }
    });
  }

###
function moutLine( p ) {
  var line = "<tr>" +
    "<td>" + p.id + "</td>" +
    "<td>" + p.name + "</td>" +
    "<td>" + p.stock + "</td>" +
    "<td>" + p.price + "</td>" +
    "<td>" + p.categories_id + "</td>" +
    "<td>" +
        '<button class="btn btn-xs btn-primary" onClick="edit(' + p.id + ')"> Editar </button> ' +
        '<button class="btn btn-xs btn-primary" onClick="remove(' + p.id + ')"> Apagar </button> ' +
    "<td>" +
    "</tr>";
  return line;
}

####
loadProducts();

####
$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': "{{ csrf_token() }}"
    }
  });
### Salvandop
$ php artisan route:list

#### Graças a rote com "resource" que criamos, temos varios "api'", e principal eh:
POST | api/produtos | App\Http\Controllers\ControladorProduct@store     

####

public function store(Request $request)
{
  $prod = new Produto();
  $prod->name = $request->input('name');
  $prod->stock = $request->input('stock');
  $prod->price = $request->input('price');
  $prod->categories_id = $request->input('categories_id');
  $prod->save();
  return json_encode($prod);
}

####
prod = { name: "mouse", stock: 10, price: 50, categories_id: 2 };

####
$.post('/api/produtos', prod, function( data ) {
   console.log(data);
});

###
$('#formProduct').submit( function(event) {
       event.preventDefault();
 console.log('teste');
});

#### Criar produtos e salvar
function createProduct() {
  prod = {
      name: $('#nameProduct').val(),
      stock: $('#amountProduct').val(),
      price: $('#priceProduct').val(),
      categories_id: $("#categorieProduct").val()
  };

  $.post( '/api/produtos', prod, function( data ) {
      product = JSON.parse( data );
      line = moutLine( product );
      $( '#tableProduct>tbody' ).append( line );
  });
}

#### Apagando

#### Add o "onclick" do edit e do remove
'<button class="btn btn-xs btn-primary" onClick="edit(' + p.id + ')"> Editar </button> ' +
'<button class="btn btn-xs btn-primary" onClick="remove(' + p.id + ')"> Apagar </button> ' +

#### Botao direito na pagina e ve o "id"
<button class="btn btn-xs btn-primary" onclick="remove(8)"> Apagar </button>


#### Dá um "find" no destroy

public function destroy($id)
{
    $prod = Product::find( $id );
    if ( isset( $prod ) ) {
        $prod ->delete();
        return Response( 'OK', 200 );
      }

      return Response('Produto nao encontrado', 404);
}

#### Função apagar, que dá um "DELETE" pra "api/produtos" com "ajax" e remover da tabela
function remove(id) {
  $.ajax({
      type: "DELETE",
      url: "/api/produtos/" + id,
      context: this,
      success: function() {
          line = $("#tableProduct>tbody>tr");
          e = line.filter( function(i, elemento) {
                return elemento.cells[0].textContent == id;
              });
          if (e)
            e.remove();
      },
      error: function() {
        console.log('erro');
      }
  });
}

#### Update
public function update(Request $request, $id)
{
    $prod = Product::find( $id );
    if ( isset( $prod ) ) {
        $prod->name = $request->input('name');
        $prod->stock = $request->input('stock');
        $prod->price = $request->input('price');
        $prod->categories_id = $request->input('categories_id');
        $prod->save();
        return json_encode($prod);

    }
    return Response('Produto nao encontrado', 404);
  }
####  Show para obter mais informacao sobre um determinado produto

public function show($id)
{
  $prod = Produto::find($id);
  if (isset($prod)) {
    return json_encode($prod);
  }
  return Response('Produto nao encontrado', 404);
}
##### Testar o Show
http://127.0.0.1:8000/api/produtos/{ID salvo}

{"id":8,"name":"asdf","stock":123423,"price":32143,"categories_id":2,"created_at":"2018-11-26 21:16:46","updated_at":"2018-11-26 21:16:46"}

####
function edit( id ) {
    $.getJSON( '/api/produtos/'+id, function( data ) {
        console.log( data );

});

#### Salvar produto e atualizar tabela

      function salveProduct() {
        prod = {
          id : $('#id').val(),
          name: $('#nameProduct').val(),
          price: $('#priceProduct').val(),
          stock: $('#amountProduct').val(),
          categories_id: $("#categorieProduct").val()
        };

        $.ajax({
            type: "PUT",
            url: "/api/produtos/" + prod.id,
            context: this,
            data: prod,
            success: function( data ) {
              prod = JSON.parse( data );
                line = $( "#tableProduct>tbody>tr" );
                e = line.filter( function( i, e ) {
                      return e.cells[ 0 ].textContent == prod.id;
                    });
                    if (e) {
                      e[0].cells[0].textContent = prod.id;
                      e[0].cells[1].textContent = prod.name;
                      e[0].cells[2].textContent = prod.stock;
                      e[0].cells[3].textContent = prod.price;
                      e[0].cells[4].textContent = prod.categories_id;
                    }
            },
            error: function() {
              console.log('error');
            }
        });
      }


##=============  Um pra um

#### Criando projeto
$ composer create-project --prefer-dist "laravel/laravel" z.Cad2
rel-um-pra-muitos


$php artisan make:model Endereco -m

#### Clientes
$table ->increments( 'id' );
$table ->string( 'name' );
$table ->string( 'tel' );


##### Endereco:

##### No relacionamento um pra um, a gente precisa ter uma chaave na tabela

$table ->integer( 'cliente_id' );
$table ->string( 'street' );
$table ->integer( 'number' );
$table ->integer( 'city' );
$table ->integer( 'uf' );
$table ->integer( 'state' );
$table ->integer( 'state' );
$table ->integer( 'zip' );


#### Esse "cliente_id" vem da tabela "clientes",
$table ->integer( 'cliente_id' )->unsigned();

#### Chave estrangeira, canoi e tabeka que ele vai referenciar
$table ->foreign( 'cliente_id' )->reference( 'id' )->on( 'clientes' );

#### transformando o "id" em uma chave primaria, pra evitar ter duplicado
$table ->primary( 'cliente_id' )

####
$table->integer('cliente_id')->unsigned();
$table->foreign('cliente_id')->references('id')->on('clientes');
$table->primary('cliente_id');
$table->string('street');
$table->integer('number');
$table->string('neighborhood');
$table->string('city');
$table->string('state');
$table->integer('zip');
$table->timestamps();


$c = new App\Cliente;
$c ->name = "renan gonsalves"
$c ->tel = 3435;
$c ->save();

####
$e = new App\Endereco;
$e ->cliente_id = 2;
$e ->street = "Rua altemar d ebarros";
$e ->number = 22;
$e ->neighborhood = "dd";
$e ->city = "dd";
$e ->zip = 22;
$e ->state = "dd";
$e ->save();


#### Utilizar o models nas rotas
use App/clientes;

#### Enderecos
Route::get('/enderecos', function () {
  $ends = Endereco::all();
  foreach ( $ends as $c ) {
      echo "<p>ID do Clientes " . $c-> cliente_id . "</p>";
      echo "<p>Rua " . $c-> street . "</p>";
      echo "<p>Numero " . $c-> number . "</p>";
      echo "<p>Bairro " . $c-> neighborhood . "</p>";
      echo "<p>Cidade " . $c-> city . "</p>";
      echo "<p>Estado " . $c-> state . "</p>";
      echo "<p>Cep " . $c-> zip . "</p>";
      echo "<hr>";
  }

});

#### lsita o endereco em clientes

Route::get('/clientes', function () {
  $clientes = Cliente::all();
  foreach ( $clientes as $c ) {
      echo "<p>ID " . $c-> id . "</p>";
      echo "<p>Nome " . $c-> name . "</p>";
      echo "<p>Telefone " . $c-> tel . "</p>";
      $e = Endereco::where( 'cliente_id', $c ->id )->first(); //tem que usar assun
      echo "<p>ID do Clientes " . $c-> cliente_id . "</p>";
        echo "<p>Rua " . $c-> street . "</p>";
        echo "<p>Numero " . $c-> number . "</p>";
        echo "<p>Bairro " . $c-> neighborhood . "</p>";
        echo "<p>Cidade " . $c-> city . "</p>";
        echo "<p>Estado " . $c-> state . "</p>";
        echo "<p>Cep " . $c-> zip . "</p>";

      echo "<hr>";
  }
});

#### Em model "clientes" buscar endereco dentro da tabela enderecos, atraves do modelo "app\Enderecos"
class Cliente extends Model
{
    public function endereco() {
        return $this->hasOne( 'App\Endereco' );
    }
}

#####
