---
title: Aprendendo Markdown
subtitle: Markdown
date: 2017-03-20
tags: ["udemy", "easy"]
---

## 3 - Cabeçalhos

#### São Seis ao todo
"# Titulo 1"
"## Titulo 2"
"### Titulo 3"
"#### Titulo 4"
"##### Titulo 5"
"###### Titulo 6"

## 4 - Paragrafos

#### Quebrando linhas(Dois espaços)

Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur,  

## 5 - Êfase

#### Negrito: Dois asteriscos "* *" no começo e no final

*"* Lorem ipsum dolor sit amet, consectetur*"*

#### Italico: Precisa apenas um asterisco
*Lorem ipsum dolor sit amet, consectetur*


#### Mesclando, italico com negrito com dois astericos e dois underline
*"*_Lorem ipsum dolor sit amet, consectetur_*"*

#### Riscado: Dois tils "~~" antes e depois

~~Lorem ipsum dolor sit amet, consectetur~~

#### Bloco de citação com ">" no inicio do paragrafo

>Lorem ipsum dolor sit amet, consecter

#### Citacao com negrito e Italico
>**Lorem** _ipsum_ dolor sit amet, consecter

#### Linhas horizontais, é com três asteriscos
***

####
